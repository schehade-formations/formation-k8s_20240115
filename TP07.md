# TP 7 - Command & Variables d'env

```bash
kubectl delete all --all -n default
mkdir ../TP07 && cd ../TP07
kubectl run my-pod --image=samiche92/my-node-server:1.0.0 -o yaml --dry-run=client > my-pod.yaml
kubectl apply -f my-pod.yaml
kubectl exec my-pod -- ps aux
vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
    command:
    - sh
    - -c
    - while true; do date; sleep 1s; done
```

```bash
kubectl delete -f my-pod.yaml && kubectl apply -f my-pod.yaml
kubectl logs -f my-pod.yaml
kubectl exec my-pod -- ps aux

kubectl exec my-pod -- printenv | less
vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
    env:
    - name: MY_VAR
      value: test
    command:
    - sh
    - -c
    - while true; do date; sleep 1s; done
```

```bash
kubectl delete -f my-pod.yaml && kubectl apply -f my-pod.yaml
kubectl exec my-pod -- printenv | less

vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
    env:
    - name: SLEEP_TIME
      value: "1"
    - name: DURATION
      value: $(SLEEP_TIME)s
    command:
    - sh
    - -c
    - while true; do date; sleep $(DURATION); done
```

```bash
kubectl delete -f my-pod.yaml && kubectl apply -f my-pod.yaml
kubectl logs -f my-pod
```
